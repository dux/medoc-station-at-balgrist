#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 15:03:59 2020

@author: frederic
"""
from   time   import  time
# numerical python, to hande everything numbers related:
import numpy  as      np

# pygame, to display stuff:
import pygame
import pygame.font

from   pyparadigm.eventlistener       import  EventListener


from    utilities       import      displayMessage, displayError, prepareDisplay,\
                                    DetectSpacebarPress, saveThresholds
from    medoc_control   import      launchMedocProgram, sendCommand
from    config          import      config
from    slider          import      recordVASWithSlider, recordPain
from    loggers         import      experimentLogger

screen = prepareDisplay()
el     = EventListener()
CROSS  = '+'

logger = experimentLogger(extra_filename='thresholds')


def launchAndMeasureTemps(ntrials, el, time_ini, offset_trials):
    temperatures = []
    for j in range(ntrials):
        displayMessage(CROSS)
        timing_starttrial = time() - time_ini
        el.wait_for_seconds(np.random.uniform(2, 10))
        
        # count down in case it's needed again:
        # for i in range(3,0, -1):
            # displayMessage(str(i))
            # el.wait_for_seconds(0.4)
            
        displayMessage(CROSS)
        sendCommand('trigger')
        keepgoing = True 
        temperature = 0
        event = None
        t0 = time()
        while keepgoing:
            if time()-t0 > 0.5:
                for event in pygame.event.get():
                    try:
                        if event.key == pygame.K_SPACE:
                            keepgoing = False
                    except:
                        pass
            if time()-t0 > 14:
                keepgoing = False

        temperature_ = sendCommand('get_status').temp
        sendCommand('no')

        if temperature_ > 0:
            temperature = temperature_
            
        logger.writeLog(
                trial_number = offset_trials + j + 1,
                timing_starttrial = timing_starttrial, 
                applied_temperature = temperature
            )
        # often the medoc station will not include a valid temperature,
        # ask or it until it gives the right value:
        temperatures.append(temperature)
        # if 3-j-1 > 0:
        #     if 3-j-1 == 1:
        #         displayMessage('Okay, 1 trial left!')
        #     else:
        #         displayMessage(f'Okay, {3-j-1} trials left!')
        # else:
        #     displayMessage(f"Done with these {ntrials}. Press space to continue.")
        #     el.wait_for_keys(pygame.K_SPACE)
        displayMessage('Danke')
        el.wait_for_seconds(1)
            
    return temperatures

def thresholds():
    # make an event listener from pyParadigm, will be used to detect
    # all the inputs: (in particular the space bar presses)
    # elspacebar = EventListener(permanent_handlers=(DetectSpacebarPress,))
    launchMedocProgram(config.thresholds)
    el.wait_for_seconds(3) # give time to the medoc station to load up.
    displayMessage("Sie erhalten nun 3 Hitzereize,\n wobei die Temperatur jeweils langsam ansteigt.\n\n  Sobald es anfängt gerade eben weh zu tun, d.h. wenn die Schmerzschwelle erreicht ist,\n drücken Sie bitte die Leertaste.\n\n Drücken Sie nun die Leertaste, um die Messung zu beginnen.")
    el.wait_for_keys(pygame.K_SPACE)
    time_ini = time()
    pain_thresholds = launchAndMeasureTemps(3, el, time_ini=time_ini, offset_trials=0)
    
    displayMessage("Sie erhalten nun weitere 3 Hitzereize.\n Drücken Sie nun bitte die Leertaste, wenn Sie keine weitere Steigerung\n der Temperatur mehr tolerieren können.\n\n Drücken Sie nun die Leertaste, um die Messung zu beginnen.")
    el.wait_for_keys(pygame.K_SPACE)
    displayMessage(CROSS)

    max_pains_tolerable = launchAndMeasureTemps(3, el, time_ini=time_ini, offset_trials=3)
    saveThresholds(np.mean(pain_thresholds), np.mean(max_pains_tolerable))
    return pain_thresholds, max_pains_tolerable
        
        
if __name__ == "__main__":
    try:
        pain_temperatures, discomfort_temperatures = thresholds()
    except Exception as e:
        print(e)
    finally:
        sendCommand('stop')
        pygame.quit()