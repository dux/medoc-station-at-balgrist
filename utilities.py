#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 14:57:40 2020

@author: fred
"""
# useful to manipulate files:
from   os.path                        import  join, exists
from   os                             import  makedirs
from   shutil                         import  copy
from   glob                           import  glob
from   random                         import  choice
from   datetime                       import  datetime
# pygame:
import pygame
import pygame.font
# paradigm, excellent interface to pygame:
from   pyparadigm.surface_composition import  compose, LinLayout, Circle, Text
from   pyparadigm.misc                import  empty_surface, display
from   pyparadigm.eventlistener       import  EventConsumerInfo
from   pyparadigm.misc                import  init
# our own stuff:
from   config                         import  config



pygame.font.init()
titlefont = pygame.font.SysFont('Arial', 50, bold=1)
mediumfont = pygame.font.SysFont('Arial', 40, bold=0)
littlefont = pygame.font.SysFont('Arial', 30, bold=0)
littleboldfont = pygame.font.SysFont('Arial', 30, bold=1)
colors = {
    'red'  : (255, 0, 0),
    'orange': (200, 100, 50),
    'green': (0, 255, 0),
    'blue' : (0, 0, 255),
    'white' : (255,255,255),
    'background' : (40,50,60),
    'lightbackground' : (100,100,100)
}

def prepareDisplay(fullscreen=False, resolution=(960, 540)):
    
    if fullscreen:
        screen = init(resolution, pygame.FULLSCREEN | pygame.HWSURFACE | pygame.DOUBLEBUF)
    else:
        screen = init((1280, 720), pygame.HWSURFACE | pygame.DOUBLEBUF)
    return screen


def displayMessage(message, shape_color=None, secondmessage=None,
                   background_color="background", otherfont=False,
                   align="center"):
    """
         using pyParadigm, displays a screen with some text at the top 
         and a square a the bottom.
         The square is made visible or not by adjusting its color. (same or 
         not as the background)
         
         Useful in the recordReactionTime routine.
    """
    if otherfont:
        font = otherfont 
    else:
        font = titlefont
    try:
        shape_color = colors[shape_color]
    except:
        pass
    try:
        background_color = colors[background_color]
    except:
        pass
    
    if secondmessage:
        secondelement = Text(secondmessage, titlefont, colors['white'])
    else:
        secondelement = Circle(shape_color, 0)
    
    
    if shape_color and message:
        composition = compose\
               (empty_surface( background_color ), LinLayout("v"))\
               (
                  Text(message, font, colors['white'], align=align),
                  secondelement
               )
    elif message and not shape_color:
        composition = compose\
               (empty_surface( background_color ), LinLayout("v"))\
               (
                  Text(message, font, colors['white'], align=align),
               )
    elif (not message) and (not shape_color):
        composition = empty_surface( background_color )
    else:
        composition = compose\
               (empty_surface( background_color ), LinLayout("v"))\
               (
                  secondelement
               )
    display(composition)

def displayError(eventlistener):
    displayMessage("There is an error, one second please :)", 'background')
    eventlistener.wait_for_keys(pygame.K_F9)
    displayMessage("Starting again.", 'background')
    eventlistener.wait_for_seconds(1.5)



def Pause(el, minseconds=30):
    displayMessage('Pause')
    el.wait_for_seconds(minseconds)
    displayMessage('Zum Fortsetzen, bitte die Leertaste drücken.')
    el.wait_for_keys(pygame.K_SPACE)
    displayMessage('')


def DetectSpacebarPress(event):
    """
        A function to be passed to an EventListener.
        This function reacts when the spacebar is pressed.
        
        Useful in the recordReactionTime routine.
    """
    if event.type == pygame.KEYDOWN :
            if event.key == pygame.K_SPACE:
                return 'pressed'
    else:
        return EventConsumerInfo.DONT_CARE
    
def readTrialsForAvoidance(list_file=config.file_with_avoidance_trials):
    with open(list_file, 'r') as f:
        lines = [l.strip().replace(' ', '') for l in f.readlines()]
    trials   = []
    for line in lines:
        if not line:
            continue
        if 'vas' in line.lower():
            trial = 'vas'
        else:
            trial = line

        trials.append(trial)
    return trials

def readIntensitiesForAvoidance(list_file=config.file_with_avoidance_intensities):
    return readTrialsForAvoidance(list_file)
    

def readTrialsForDiscrimination(list_file=config.file_with_discrimination_trials):
    trials = readTrialsForAvoidance(list_file)
    return trials


def makeFilenameForSequenceOfRewards(pooldir):
    timenow  = datetime.strftime(datetime.now(), '_%Y_%m_%d_%H_%M_%S')
    filename = join(pooldir, config.participant_code+timenow)
    return filename

def saveSequenceOfRewards(sequence, pooldir, filename=None):
    if not exists(pooldir):
        makedirs(pooldir)
    if not filename:
        filename = makeFilenameForSequenceOfRewards()
    
    with open(filename, 'a+') as f:
        for e in sequence:
            f.write(str(int(bool(e)))+'\n')
            
def loadSequenceOfReward(pooldir, filename=None, avoid_same_participant=True):
    allfiles = glob(join(pooldir, '*'))
    if filename:
        with open(join(pooldir, filename)) as f:
            lines = f.readlines()
        sequence = [bool(int(e.strip())) for e in lines]
        
    else:
        if avoid_same_participant:
            allfiles = [f for f in allfiles if not config.participant_code == f.split('_')[0]]
        toload = choice(allfiles)
        with open(toload, 'r') as f:
            lines = f.readlines()
        sequence = [bool(int(e.strip())) for e in lines]
    return sequence

class recordEntry():
    def __init__(self, pain_threshold, max_pain_tolerable, adjusted_temperature):
        self.set('pain_threshold', pain_threshold)
        self.set('max_pain_tolerable', max_pain_tolerable)
        self.set('adjusted_temperature', adjusted_temperature)
        
    def set(self, attr, value):
        try:
            value = f"{float(value):.02f}"
        except:
            value = 'NA'
        setattr(self, attr, value)
    def set_all(self, pain_threshold, max_pain_tolerable, adjusted_temperature):
        self.set('pain_threshold', pain_threshold)
        self.set('max_pain_tolerable', max_pain_tolerable)
        self.set('adjusted_temperature', adjusted_temperature)
        
def loadRecordOfThresholds(participant_code=config.participant_code):
    # just a named tuple not to get confused with the two in the future
    if not exists(config.record_of_thresholds):
        return {}
    with open(config.record_of_thresholds, 'r') as f:
        lines = f.readlines()
    entries = {}
    for line in lines[1:]: #skip the header
        line = line.strip()
        if line == '':
            continue
        participant, threshold, maxpain, adjusted_temp = line.split('\t')
        try:
            adjusted_temp = float(adjusted_temp)
        except:
            pass
        entries[participant] = recordEntry(threshold, maxpain, adjusted_temp)
    if participant_code:
        if participant_code in entries:
            print(f"Loading thresholds for participant {participant_code}")
            return entries[participant_code]
        else:
            raise Exception(f"No pain thresholds were found for participant {participant_code} in the file {config.record_of_thresholds}.")

    return entries

def saveThresholds(pain_threshold=None, max_pain_tolerable=None, adjusted_temperature=None,
                   participant=config.participant_code):
            
    entries = loadRecordOfThresholds(participant_code=None)
    if participant in entries:
        entry = entries[participant]
        pain_threshold       = pain_threshold or entry.pain_threshold
        max_pain_tolerable   = max_pain_tolerable or entry.max_pain_tolerable
        adjusted_temperature = adjusted_temperature or entry.adjusted_temperature
    entry = recordEntry(pain_threshold, max_pain_tolerable, adjusted_temperature)
    entries[participant] = entry
    
    madebackup = False
    backup = config.record_of_thresholds+'.backup'
    if exists(config.record_of_thresholds):
        copy(config.record_of_thresholds, backup)
        madebackup = True
    with open(config.record_of_thresholds, 'w') as f:
        # header:
        f.writelines(['\t'.join(['participant_code', 'pain_threshold', 'max_pain_tolerable', 'adjusted_temperature\n'])])
        for participant, entry in entries.items():
            pain_threshold = entry.pain_threshold
            max_pain_tolerable = entry.max_pain_tolerable
            adjusted_temperature = entry.adjusted_temperature
            to_write = '\t'.join([participant, pain_threshold, max_pain_tolerable, adjusted_temperature+'\n'])
            f.writelines([to_write])
    def fileLen(path):
        with open(path) as f:
            for i, _ in enumerate(f):
                pass
        return i + 1
    if madebackup and fileLen(config.record_of_thresholds) < fileLen(backup):
        copy(backup, backup.replace('backup', 'iamasafecopy.txt'))
    
if __name__ == "__main__":
    # pass
    # saveThresholds(49.2, 55.2)
    # saveThresholds(participant='didnotparticipate')
    # print(readTrialsForAvoidance())
    screen = prepareDisplay()
    displayMessage('hello', shape_color=None)
    displayMessage("", 'blue')
    displayMessage('', background_color='lightbackground')