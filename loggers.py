#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 16:36:52 2020

@author: fred
"""
from    os.path  import    exists
import  logging
from    datetime import    datetime
import  inspect
import  numpy    as        np


from    config   import    config
  
formatter = logging.Formatter('%(asctime)s.%(msecs)02d | %(levelname)s | %(message)s', \
                              datefmt='%d-%b-%y %H:%M:%S')

"""
participant code
date

time

session_number
trial_number
condition
answer
answer_time
reaction time
reward
timing_starttrial
timing_cue
timing_target
timing_response
timing_oucome
applied_temperature
VAS_intensity VAS_unpleasantenss
comments
"""

class experimentLogger():
    def __init__(self, 
                 participant_code=config.participant_code,
                 session_number=config.session_number,
                 outfile=None,
                 extra_filename=None):
        self.participant_code = participant_code
        self.session_number   = session_number
        self.sep              = '\t' # the separator is a tab
        if not outfile:
            outfile = f"participant_{participant_code}-session_{session_number}.log"
        self.outfile = outfile
        if extra_filename:
            self.outfile = self.outfile.replace('.log', f'_{extra_filename}.log')
    def interpretNumber(self, answer, ndigits):
        if answer == 'NA':
            return 'NA'
        try:
            answer = f"{answer:.{ndigits}f}"
        except:
            answer = str(answer)
        return answer
            
    def writeLog(self, 
                 trial_number='',
                 condition='', 
                 answer='', 
                 answer_time='', 
                 reaction_time='', 
                 limit_time='',
                 reward='',
                 timing_starttrial='',
                 timing_cue='',
                 timing_target='',
                 timing_response='',
                 timing_outcome='',
                 applied_temperature='',
                 VAS_intensity='',
                 VAS_unpleasanteness='',
                 comments=''):
        if not exists(self.outfile):
            self.makeHeader()
        now  = datetime.now()
        day  = datetime.strftime(now,"%Y-%m-%d")
        time = datetime.strftime(now, "%H:%M:%S.%f"[:-3])
        row  = [str(self.participant_code)]
        row.append(str(day))
        row.append(str(time))
        row.append(str(self.session_number))
        row.append(str(trial_number))
        row.append(str(condition))
        row.append(str(answer))
        row.append(self.interpretNumber(answer_time, 3))
        row.append(self.interpretNumber(reaction_time, 3))
        row.append(self.interpretNumber(limit_time, 3))
        try:
            # make sure we always have 0 or 1 in the reward column, not "True"/"False"
            reward = int(reward)
        except:
            pass
        row.append(str(reward))
        row.append(self.interpretNumber(timing_starttrial, 3))
        row.append(self.interpretNumber(timing_cue, 3))
        row.append(self.interpretNumber(timing_target, 3))
        row.append(self.interpretNumber(timing_response, 3))
        row.append(self.interpretNumber(timing_outcome, 3))
        row.append(self.interpretNumber(applied_temperature, 2))
        row.append(self.interpretNumber(VAS_intensity, 1))
        row.append(self.interpretNumber(VAS_unpleasanteness, 1))
        row.append(str(comments))
        self.writeOneRow(row)
        
    def makeHeader(self):
        alwaysargs = ['participant_code', 'date', 'time', 'session_number']
        moreargs   = [arg for arg in inspect.getfullargspec(self.writeLog).args if not arg == 'self']
        allargs    = alwaysargs + moreargs
        self.writeOneRow(allargs, console=False)
        
    def writeOneRow(self, row: list, console:bool=True):
        strtowrite = self.sep.join([str(element) for element in row]) + '\n'
        with open(self.outfile, 'a+') as f:
            f.write(strtowrite)
        if console:
            print(strtowrite)
            
    def finalize(self):
        """ 
             basically remove the empty columns for better readability 
        """
        with open(self.outfile, 'r') as f:
            lines = f.readlines()
        includes = []
        for line in lines:
            line = line.split('\t')
            include = [False if e == '' else True for e in line ]
            includes.append(include)
        includes = np.array(includes)[1:, :]
        notempty = np.where(np.max(includes, axis=0))
        with open(self.outfile, 'w') as f:
            for line in lines:
                line = line.split('\t')
                line = [line[i] for i in notempty[0]]
                f.write('\t'.join(line))
        

def setup_logger(name, log_file, level=logging.INFO):

    handler = logging.FileHandler(log_file)        
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.handlers = []
    logger.setLevel(level)
    logger.addHandler(handler)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(formatter)
    logger.addHandler(consoleHandler)

    return logger

medocname = f"{config.participant_code}_medoc"
medoc_logger = setup_logger(medocname, medocname+'.log')

    
if __name__ == "__main__":
    test = experimentLogger(outfile='test.log')
    test.writeLog(trial_number=1,
                  condition='ok', 
                  answer='NA', 
                  answer_time=0.3, 
                  reaction_time=3,
                  limit_time=5,
                  reward='NA', 
                  timing_starttrial="bad m'kay?", 
                  timing_cue=2.6,
                  timing_target=2.9, 
                  timing_response=3.1, 
                  timing_outcome=3.5, 
                  applied_temperature=42.0, 
                  VAS_intensity=120, 
                  VAS_unpleasanteness=150, 
                  comments='a comment')
    test.writeLog(trial_number=2,
                  condition='ok', 
                  answer='NA', 
                  answer_time=0.3, 
                  reward='NA', 
                  timing_starttrial="bad m'kay?", 
                  timing_cue=2.6,
                  timing_target=2.9, 
                  timing_response=3.1, 
                  timing_outcome=3.5, 
                  applied_temperature=42.0, 
                  VAS_intensity=120, 
                  VAS_unpleasanteness=150, 
                  comments='a comment')
    test.finalize()





# experimentname = f"{config.participantid}_experimentdata"
# experiment_logger = setup_logger(experimentname, experimentname+'.log')
