#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 16:30:57 2020

@author: Frédéric Dux
"""

from   time                           import   time
from   datetime                       import   datetime
import socket
import struct
from   pyparadigm.eventlistener       import   EventListener

from   config   import   config
from   loggers  import   medoc_logger 


##############################################################################
##############################################################################
##############################################################################
command_to_id = {
    'GET_STATUS':   0,
    'SELECT_TP':    1,
    'START':        2,
    'PAUSE':        3,
    'TRIGGER':      4,
    'STOP':         5,
    'ABORT':        6,
    'YES':          7,  # used to start increasing the temperature
    'NO':           8,  # used to start decreasing the temperature
    'COVAS':        9,
    'VAS':         10,
    'T_UP':        12,
    'T_DOWN':      13,
    'KEYUP':       14,  # used to stop the temperature gradient,
}

# make the same as above but reversed:
id_to_command = {item:key for key, item in command_to_id.items()}

test_states = { 
    0   : 'IDLE',
    1   : 'RUNNING',
    2   : 'PAUSED',
    3   : 'READY'
}

states = {
    0   : "IDLE",
    1   : "READY",
    2   : "TEST IN PROGRESS"    
}

response_codes = { 0 :    "OK",
                   1 :    "FAIL: ILLEGAL PARAMETER",
                   2 :    "FAIL: ILLEGAL STATE TO SEND THIS",
                   3 :    "FAIL: NOT THE PROPER TEST STATE",
                   4096:  "DEVICE COMMUNICATION ERROR", 
                   8192:  "safety warning, test continues",
                   16384: "Safety error, going to IDLE"
}


##############################################################################
##############################################################################
##############################################################################

# pretty printer:
class medocResponse():
    """
       Just a container to interpret and store whatever the station sends towards
       us. This is the receiver.
    """
    # decoding the bytes we receive:
    def __init__(self, response):
        self.length    = struct.unpack_from('H', response[0:4])[0]
        self.timestamp = socket.ntohl(intFromBytes(response[4:8]))
        
        # make the timestamp more pretty:
        self.datetime  = datetime.fromtimestamp(self.timestamp)
        self.strtime   = self.datetime.strftime("%Y-%m-%d %H:%M:%S")
        
        self.command   = intFromBytes(response[8:9])
        
        self.state     = intFromBytes(response[9:10])
        # see if we have a documented state for this response:
        if self.state in states:
            self.statestr = states[self.state]
        else:
            self.statestr = 'unknown state code'
        
        self.teststate = intFromBytes(response[10:11])
        # see if we have a documented test state for this response:
        if self.teststate in test_states:
            self.teststatestr = test_states[self.teststate]
        else:
            self.teststatestr = 'unknown test state code'
        
        
        self.respcode  = struct.unpack_from('H', response[11:13])[0]
        # same as usual, see if we have something corresponding to this response code:
        if self.respcode in response_codes:
            self.respstr   = response_codes[self.respcode]
        else:
            self.respstr    = "unknown response code"
        
        # the test time is in seconds once divided by 1000:
        self.testtime  = struct.unpack_from('I', response[13:17])[0] / 1000.
        # the temperature is in °C once divided by 100:
        self.temp      = struct.unpack_from('h', response[17:19])[0] / 100.
        self.CoVAS     = intFromBytes(response[19:20])
        self.yes       = intFromBytes(response[20:21])
        self.no        = intFromBytes(response[21:22])
        self.message   = response[22:self.length]
        # store the whole response as well just in case:
        self.response  = response
        
    # some more structure to make it nicer when we print it in the terminal:
    def __repr__(self):
        msg = ""
        msg += f"timestamp     : {self.strtime}\n"
        msg += f"command       : {id_to_command[self.command]}\n"
        msg += f"state         : {self.statestr}\n"
        msg += f"test state    : {self.teststatestr}\n"
        msg += f"response code : {self.respstr}\n"
        msg += f"temperature   : {self.temp}°C\n"
        if   self.statestr == "TEST IN PROGRESS":
            msg += f"test time     : {self.testtime} seconds\n"
        elif self.respstr != "OK":
            msg += f"sup. message  : {self.message}\n"
        if self.yes:
            msg += "~~ also: yes was pressed! ~~\n"
        if self.no:
            msg += "~~ also: no was pressed! ~~\n"
        return msg
    def __str__(self):
        return self.__repr__()
    def __getitem__(self, s):
        return self.response[s]


def sendCommand(command, parameter=None, address=config.address, port=config.port, 
                el=EventListener()):
    """
         this is the main piece, this functions allows us to send commands to the
         station.
         e.g. :      sendCommand('get_status')   
         or          sendCommand('select_tp', '01000000')
    """
    # convert our command to bytes:
    commandbytes = commandBuilder(command, parameter=parameter)
    if config.debug:
        print(f'Sending the following bytes: {commandbytes} -- {len(commandbytes)} bytes')
    # now the connection part:
    for attemps in range(3):
        try:
    	    s = socket.socket()
    	    s.connect((address, port))
    	    s.send(commandbytes)
    	    data = msg = s.recv(1024)
    	    while data:
    	    	data = s.recv(17)
    	    	msg += data
    	    resp = medocResponse(msg)
    	    if config.debug:
    	    	print("Received: ")
    	    	print(resp)
    	    break
        except ConnectionResetError:
            print("==> ConnectionResetError")
            attemps += 1
            s.close()
            el.wait_for_seconds(config.timedelayformedoc)
            pass
    # now wait a bit, don't want to overstress the medoc station (safety):
    el.wait_for_seconds(config.timedelayformedoc)
    return resp


#  converters from bytes to int and reverse:
def intToBytes(integer, nbytes):
    return integer.to_bytes(nbytes, byteorder='big')
def intFromBytes(xbytes):
    return int.from_bytes(xbytes, 'big')


# packs bytes together according to the pdf documentation:
def commandBuilder(command, parameter=None):
    if type(command) is str:
        command = command_to_id[command.upper()]
        
    if type(parameter) is str:
        # then program code, e.g. '00000001'
        parameter = int(parameter, 2)
    elif type(parameter) is float:
        parameter = 100*parameter
       
    commandbytes = intToBytes(socket.htonl(int(time())), 4)    
    commandbytes += intToBytes(int(command), 1)
    if parameter:
	    commandbytes += intToBytes(socket.htonl(int(parameter)), 4)
    return intToBytes(len(commandbytes), 4) + commandbytes # prepending the command data with 4-bytes header that indicates the command data length


# a function to launch programs on the medoc station.
def launchMedocProgram(program_code, eventlistener=EventListener(), go_past_pretest=True):
    """
     send messages to the station until it is in the right state.
     Often the station will crash because of a bad handling
     of some cases on their side. In this case, one needs to
     aknowledge the error but not quit the application!
     No need for a restart, just press "no" and it will continue its business.

    Parameters
    ----------
    program_code : string
        string representation of the 8 bits associated with a program 
        in the medoc station setting, e.g. 01000000
    eventlistener : pyparadigm.eventlistener, optional
        a pyparadigm instance of an eventlistener to still have
        control over the script when waiting for the medoc station
        to answer. The default is None, in this case we make a fake
        event listener that will not listen during the wait times.
    go_past_pretest : bool, optional
        force all the way passed the pre-test.

    Returns
    -------
    int
        a status
          0 for no problem
          1 for connection error
          2 for other errors
    """
    
    try:
        if not canWeConnectToTheStation():
            raise ConnectionRefusedError
            eventlistener.wait_for_seconds(config.timedelayformedoc)
        # if a test is already running, stop it:
        if sendCommand('get_status').teststatestr != 'IDLE':
            sendCommand('stop')
            eventlistener.wait_for_seconds(config.timedelayformedoc)
            sendCommand('stop')
            eventlistener.wait_for_seconds(config.timedelayformedoc)
        # wait until the station is in its idle state:
        resp = sendCommand('get_status')
        while resp.statestr == "TEST IN PROGRESS" :
            eventlistener.wait_for_seconds(config.timedelayformedoc)
            resp = sendCommand('get_status')
        # select the program:
        medoc_logger.info(f"Launching a program: {program_code}.")
        eventlistener.wait_for_seconds(config.timedelayformedoc)
        sendCommand('select_tp', program_code)
        return 0
        
    except ConnectionRefusedError:
        return 1
    except Exception as e:
        medoc_logger.error(f"Error in launchMedocProgram: {e}")
        sendCommand('stop')
        eventlistener.wait_for_seconds(config.timedelayformedoc)
        sendCommand('abort')
        return 2
    
def holdMedocStation(*statuses, eventlistener=EventListener()):
    """ 
        holds everything as long as the medoc station displays a status
        that matches one of those passed in the arguments
    """
    try:
        if not canWeConnectToTheStation():
            raise ConnectionRefusedError
        resp = sendCommand('get_status')
        while (resp.teststatestr in statuses) or (resp.statestr in statuses):
            eventlistener.wait_for_seconds(config.timedelayformedoc)
            resp = sendCommand('get_status')
        return 0
    except ConnectionRefusedError:
        return 1
    except Exception as e:
        medoc_logger.error(f"Error in holdMedocStation: {e}")
        sendCommand('stop')
        eventlistener.wait_for_seconds(config.timedelayformedoc)
        sendCommand('abort')
        return 2

def makeTemperaturePlateau(degrees, durationattheplateau, eventlistener=EventListener()):
    """
         this will send the sequence of command that will increase the temperature
         for a moment to the medoc station.
    """
    # using their weird protocol of starting a temperature ramp by sending 
    # "yes", and stopping int by sending "keyup".
    # "no" starts a negative ramp and is used to go back
    # to the original temperature.
    # all this is prone to the uncertainty of the time of reception
    # of the messages. There seems to me no mean of obtaning the
    # temperature from the messages that are sent to us by the station,
    # thus we can only trust it does the right thing.

    ramptime = degrees / config.temperatureRate
    if degrees == 0:
        empirical_extra_wait_time = 0.6 # compensates for the delays due to the commands
        eventlistener.wait_for_seconds(durationattheplateau + 2*ramptime + empirical_extra_wait_time)
        return
    temperature_before = sendCommand('yes').temp
    eventlistener.wait_for_seconds(ramptime)
    sendCommand('keyup')
    eventlistener.wait_for_seconds(durationattheplateau / 2.)
    temperature_during = sendCommand('get_status').temp
    eventlistener.wait_for_seconds(durationattheplateau / 2.)
    sendCommand('no')
    eventlistener.wait_for_seconds(ramptime)
    temperature_after = sendCommand('keyup').temp
    logmsg = f"Made a temperature plateau {temperature_before} -> {temperature_during} ({durationattheplateau} second) -> {temperature_after}. "
    logmsg+= f"(assumed rate: {config.temperatureRate} degrees per second)"
    medoc_logger.info(logmsg)
    return temperature_before, temperature_during, temperature_after


def canWeConnectToTheStation(address=config.address, port=config.port):
    s = socket.socket()
    try:
        s.connect((address, port))
        return 1
    except Exception as e:
        message  = f"The instrument at {address}:{port} is not responding. "
        message += f"The following error was received: {e}"
        medoc_logger.error(message)
        return 0
    finally:
        s.close()

