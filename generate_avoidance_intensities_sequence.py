#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 22:57:28 2020

@author: frederic
"""


from config import config
from numpy.random import choice


def Ok(sequence, choices):
    for e in choices:
        c = len([i for i in sequence if i == e])
        if c != 75:
            return False
    return True


choices = ['niedrig', 'hoch']
sequence = []

while not Ok(sequence, choices):
    sequence = []
    for i in range(150):
        toadd = choice(choices)
        if i>=3:
            prev = sequence[i-1]
            pprev = sequence[i-2]
            ppprev = sequence[i-3]
            if prev == pprev == ppprev == toadd:
                toadd = choices[0]
            if prev == pprev == ppprev == toadd:
                toadd = choices[1]
            
        sequence.append(toadd)

with open(config.file_with_avoidance_intensities, 'r') as f:
    lines = f.readlines()

with open(config.file_with_avoidance_intensities, 'w') as f:
    for e, l in zip(sequence, lines):
        l = l.strip()
        if ':' in l:
            _, extra = l.split(':')
            newline = e + ' : ' + extra
        else:
            newline = e
        f.write(newline+'\n')
    if len(sequence)>len(lines):
        for i in range(len(lines), len(sequence)):
            newline = sequence[i]
            f.write(newline + '\n')
            
        