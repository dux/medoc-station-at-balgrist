#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  1 18:02:46 2020

@author: fred
"""
from   time   import  time
# numerical python, to hande everything numbers related:
import numpy  as      np

# pygame, to display stuff:  
import pygame
import pygame.font

from   pyparadigm.eventlistener       import  EventListener


# our very own ways of controlling the medoc station:
from   medoc_control                  import  launchMedocProgram, makeTemperaturePlateau,\
                                              sendCommand, holdMedocStation
# our parameters to the experiment:
from   config                         import  config
# our loggers:
from   loggers                        import  experimentLogger
# our other functions:
from   utilities                      import  displayMessage, displayError,\
                                              readTrialsForDiscrimination,\
                                              saveSequenceOfRewards, \
                                              loadSequenceOfReward, \
                                              prepareDisplay, Pause, \
                                              makeFilenameForSequenceOfRewards
from   slider                         import  recordPain, recordDiscomfort

######################## INITIALIZATION OF THE SCREEN #########################

# make a logger:
logger = experimentLogger(extra_filename='discrimination')
# the participant id, session number are taken from config.txt. They can also be
# given as parameters: experimentLogger(participant_code='my code', session_number=42) ...
reward_sentence = "Gewonnen! \n 0.20CHF"

CROSS = "+"
bigfont = pygame.font.SysFont('Arial', 250, bold=1)


# filename where to save the sequence of successes:
pooldir  = "discrimination_reward_sequence_pool"
saved_sequence_of_reward_filename = makeFilenameForSequenceOfRewards(pooldir)

def launchTrial(el):
    sendCommand('stop')
    el.wait_for_seconds(2)
    sendCommand('stop')
    el.wait_for_seconds(config.timedelayformedoc)
    holdMedocStation('RUNNING')
    el.wait_for_seconds(config.timedelayformedoc)
    resp = launchMedocProgram(config.discrimination, eventlistener=el,\
                              go_past_pretest=False)
    while resp != 0:
        print(resp)
        displayError(el)
        resp = launchMedocProgram(config.discrimination, eventlistener=el)
    temp = sendCommand('get_status').temp
    while temp < 42.5:
        print(f'   holding, temperature ({temp}) has not reached the baseline yet (43)')
        el.wait_for_seconds(config.timedelayformedoc)
        temp = sendCommand('start').temp

def discrimination(trials, reinforcement_trial=-1, determined_reward_sequence="None",
                   start_trial=0):
    """
     the discrimination experiment

    Parameters
    ----------
    trials : list
        list of temperature offsets in °C.

    """
    # maybe we will need to record the sequence of successes ~ aka rewards
    # we'll store of sequence of successes if determined_reward_sequence is 'None'
    successes = []
    # as usual, we make an event listener to control everything:
    el = EventListener()
    
    cumulative_reward_save = f"cumulative_reward_discrimination_{config.participant_code}.txt"
    try:
        cumulative_reward = float(np.loadtxt(cumulative_reward_save))
    except:
        cumulative_reward = 0.
    
    # keep track of the very beginning of the experiment:
    time_beginning_of_experiment = time()
    message = """
Im Folgende erhalten Sie wiederholt Hitzereize. Dabei erhöht sich zunächst
die Temperatur und bleibt dann für ein paar Sekunden konstant. 
Danach kann es sein, dass sich die Temperatur nochmals erhöht, 
wobei diese Änderungen sehr klein sein können. 

Ihre Aufgabe ist es, herauszufinden, ob sich die Temperatur nochmals erhöht 
hat oder nicht. Kurz bevor sich die Temperatur erhöhen kann, 
wird Ihnen ein Ausrufezeichen als Hinweis angezeigt.

Sie werden dann gefragt, ob Sie eine Temperaturerhöhung gespürt habe oder nicht. 
Um „JA“ zu antworten, drücken Sie bitte die Pfeiltaste nach links, 
um „NEIN“ zu antworten, drücken Sie bitte die Pfeiltaste nach rechts.

Zum Fortsetzen, bitte die Leertaste drücken.
"""
    displayMessage(message)
    el.wait_for_keys(pygame.K_SPACE)
    message2 = """
Nach einer Weile werden Sie in manchen Durchgängen Geldgewinne erhalten. 
Sollte dies der Fall sein, werden Ihnen diese nach Ihrer Reaktion auf dem
Bildschirm angezeigt.

Hin und wieder werden Sie gebeten, einzuschätzen, wie intensitv und
wie angenehm oder unangenehm Sie die Hitzereize empfinden. 
Dazu erhalten Sie den Hitzereize einmal ohne zusätzliche Temperaturerhöhung.
und Ihnen werden nacheinander die beiden Skalen angezeigt. 
Danach geht die Aufgabe weiter.

Drücken Sie die Leertaste, um die Aufgabe zu beginnen.
"""
    displayMessage(message2)
    el.wait_for_keys(pygame.K_SPACE)
    i = max(0, start_trial - 1)
    Ntrials = len(trials)
    displayMessage("")
    is_it_time_for_break = time()
    while i < Ntrials:
        # check if it is time for a break:
        if (time() - is_it_time_for_break) / 60 > 15 and (Ntrials-i)>10:
            Pause(el, minseconds=30)
            is_it_time_for_break = time()
        timing_starttrial = time() - time_beginning_of_experiment
        temp_offset = trials[i]
        if temp_offset == 'vas':
            displayMessage('', background_color='lightbackground')
            launchTrial(el)
            el.wait_for_seconds(10)
            sendCommand('stop')
            el.wait_for_seconds(0.5)
            VAS_intensity = recordPain(screen, el, background_color='lightbackground')
            el.wait_for_seconds(0.1)
            VAS_unpleasanteness = recordDiscomfort(screen, el, background_color='lightbackground')
            logger.writeLog(trial_number=i+1,
                VAS_intensity=VAS_intensity,
                VAS_unpleasanteness=VAS_unpleasanteness,
                comments='vas only')
            i += 1
            continue # skip the rest of the trial.
        else:
            temp_offset = float(temp_offset)
        # start the medoc trial:
        launchTrial(el)
        el.wait_for_seconds(4) 
        displayMessage("!", otherfont=bigfont)
        timing_cue = time() - time_beginning_of_experiment
        el.wait_for_seconds(0.5)
        displayMessage(CROSS)
        wait_time = np.random.randint(500, 1500)/1000.
        el.wait_for_seconds(wait_time) 
        timing_target = time() - time_beginning_of_experiment
        if abs(temp_offset) > 0:
            makeTemperaturePlateau(temp_offset, durationattheplateau=1, eventlistener=el)
        else:
            # with temp_offset = 0 exactly, this will simply wait without sending a command.
            makeTemperaturePlateau(0, durationattheplateau=1, eventlistener=el)
        el.wait_for_seconds(0.5)
        msg = " Haben Sie einen Temperaturanstieg gespürt?\n\n\n "
        msg+= "← Ja          Nein → "
        displayMessage(msg)
        t0 = time()
        # the participant has 2 seconds to answer:
        answer = el.wait_for_keys(pygame.K_LEFT, pygame.K_RIGHT, timeout=2.0)
        displayMessage(CROSS)
        answer_time = time() - t0
        comment = ''
        if answer:
            answer = 'yes' if answer == pygame.K_LEFT else 'no'
            timing_response = time() - time_beginning_of_experiment
        else:
            answer = 'NA'
            timing_response = 'NA'
            answer_time = 'NA'
            comment += 'no answer within 2 seconds'
        sendCommand('stop')
        
        # first determined if the answer was correct:
        if (answer == 'yes' and abs(temp_offset) > 0) or (answer == 'no' and abs(temp_offset) == 0):
            msg     = ""
            success = True
        elif answer == 'NA':
            msg    = "" 
            success = False
        else:
            msg    = ""
            success = False
        
        # store the success for use in another experiment:
        if i < reinforcement_trial:
            saveSequenceOfRewards([success], pooldir, filename=saved_sequence_of_reward_filename)
        
        if reinforcement_trial >= 0 and i >= reinforcement_trial:
            # if we do reinforcement, we may or not receive a reward
            # depending on the phase of the trial.
            # Either the rewards are given based on a sequence:
            if determined_reward_sequence != "None":
                reward = determined_reward_sequence[i-reinforcement_trial]
            else: 
                # or the reward is simply based on the success
                reward = success 
                # in this case, we also record the sequence of successes
                # so that we can use it on other participants as a subsitute
                # for a true random sequence of rewards:
                successes.append(success)
        else: # if we don't do reinforcement, we never hand out rewards
            reward = 0
        # so we now complete the message to be displayed:
        if reward:
            msg += reward_sentence
            cumulative_reward += 0.2 
            np.savetxt(cumulative_reward_save, [cumulative_reward])
        timing_outcome = time() - time_beginning_of_experiment
        if msg:
            displayMessage(msg)
            # if the message is not empty, display it. (if not, keep the cross)
        
            
        logger.writeLog(trial_number=i+1,
                        answer=answer,
                        answer_time=answer_time,
                        reward=reward,
                        cumulative_reward=cumulative_reward,
                        timing_starttrial=timing_starttrial,
                        timing_cue=timing_cue,
                        timing_target=timing_target,
                        timing_response=timing_response,
                        timing_outcome=timing_outcome,
                        applied_temperature=temp_offset,
                        comments=comment,
                        VAS_intensity='NA',
                        VAS_unpleasanteness='NA'                       
                        )
        el.wait_for_seconds(4)
        
        if not answer == "NA": 
            # we "repeat" this trial if no answer was given in time -- remain at the same trial number.
            i += 1
        if i < Ntrials:
            displayMessage('Gleich beginnt der nächste Durchgang')
        el.wait_for_seconds(1)
        displayMessage('+')
        
    displayMessage("Danke")
    
    el.wait_for_seconds(1)
    sendCommand('stop')
    
if __name__ == "__main__":
    screen = prepareDisplay()
    try:
        # load the sequence of trials from discrimination_trials.txt:
        trials_sequence = readTrialsForDiscrimination()
        number_of_trials_before_reinforcement = config.discrimination_number_of_trials_before_reinforcement
        
        # do we use a predetermined sequence?
        usepredeterminedsequence = config.discrimination_use_predetermined_sequence
        if usepredeterminedsequence.lower() == 'yes':
            predeterminedsequence = loadSequenceOfReward(pooldir, config.discrimination_file_with_predetermined_sequence)
        else:
            predeterminedsequence = "None"
        
        discrimination(trials=trials_sequence, \
                       reinforcement_trial=number_of_trials_before_reinforcement,\
                       determined_reward_sequence=predeterminedsequence,\
                       start_trial=config.discrimination_start_trial)
        
    except Exception as e:
        print(e)
        import traceback
        traceback.print_exc()
    finally:
        sendCommand('stop')
        pygame.quit()
        
