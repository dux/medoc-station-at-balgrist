#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 15:03:55 2020

@author: frederic
"""


from   time   import  time
# numerical python, to hande everything numbers related:
import numpy  as      np

# pygame, to display stuff:
import pygame
import pygame.font

from   pyparadigm.eventlistener       import  EventListener


from    utilities       import      displayMessage, displayError, prepareDisplay,\
                                    DetectSpacebarPress, saveThresholds,\
                                    loadRecordOfThresholds
from    medoc_control   import      launchMedocProgram, sendCommand
from    config          import      config
from    slider          import      recordPain
from    loggers         import      experimentLogger

screen = prepareDisplay()
logger = experimentLogger(extra_filename='adjustment')

TARGET_VAS = config.adjustment_target_vas

RATE   = config.temperatureRateAdjustment
LENGTH = config.adjustment_plateau_length
CROSS  = '+'
BASELINE = config.adjustement_baseline_temperature # °C

def regulateTemperature(target, el):
    """
       This is not useful anymore. It was when temperature
       had to be set with the yes/no commands.
    """
    current_temp = 0 
    while current_temp == 0:
        current_temp = sendCommand('get_status').temp 
    diff = target-current_temp
    print(diff)
    if diff < 0:
        mode = 'T_down'
    elif diff > 0:
        mode = 'T_up'
    else:
        return current_temp 
    
    sendCommand(mode, abs(diff))
    """
    current_temp = 0
    while abs(current_temp - target) > 0.5:
        while current_temp == 0:
            current_temp = sendCommand('get_status').temp 
        ramptime = (target-current_temp) / config.temperatureRateAdjustment
        command = 'yes' if ramptime > 0 else 'no'
        sendCommand(command)
        el.wait_for_seconds(abs(ramptime))
        current_temp = sendCommand('keyup').temp
    return current_temp
    """
    
def makeTemperaturePlateau(t_diff, T_reached, plateau_length, rate, el):
    # 2 times the slew to the new temp, plateau length, and 1 extra second as requested
    wait_time = 2*abs(T_reached-BASELINE)/rate + plateau_length + 1
    if t_diff <= 0:
        sendCommand('t_down', abs(t_diff))
        pass
    else:
        sendCommand('t_up', t_diff)
    
    el.wait_for_seconds(wait_time)
    temp = sendCommand('get_status').temp 
    wait_extra = False
    while temp > BASELINE + 0.5:
        wait_extra = True
        temp  = sendCommand('get_status').temp 
        el.wait_for_seconds(0.1)
        print(f"Temperature not returned to baseline yet: {temp:.01f}°C")
    if wait_extra:
        el.wait_for_seconds(1)

def adjustement():
    displayMessage('(loading)')
    launchMedocProgram(config.adjustment)
    el = EventListener()
    # give the station time to initialize:
    el.wait_for_seconds(2)
    
    thresholds     = loadRecordOfThresholds()
    pain_threshold = float(thresholds.pain_threshold)
    max_pain_tolerable = float(thresholds.max_pain_tolerable)
    temp_adjusted = 0.7*pain_threshold + 0.3*max_pain_tolerable
    
    message  = "Sie erhalten im Folgende wiederholte Hitzereize. Während Sie diese Hitze spüren sollen Sie\n"
    message += "gleichzeitig angeben, wie intensiv sich diese anfühlt. Benutzen Sie dazu bitte die Skala, die\n"
    message += "auf dem Bildschirm angezeigt wird. Während der Stimulation kann es sein, dass sich die\n"
    message += "Intensität ändern. Bitte geben Sie dies entsprechend auf der Skala an, solange diese auf dem\n"
    message += "Bildschirm zu sehen ist.\n\n"
    message += "Drücken Sie die Leertaste, um zu beginnen."
    displayMessage(message)
    el.wait_for_keys(pygame.K_SPACE)
    displayMessage(CROSS)
    
    time_beginning_of_experiment = time()
    trial_counter = 1
    VASes    = []
    deltaT   = temp_adjusted-BASELINE
    makeTemperaturePlateau(deltaT, temp_adjusted, LENGTH, RATE, el)
    VAS      = recordPain(screen, el, maxtime=3)
    VASes.append(VAS)
    displayMessage('Gleich startet der nächste Reiz')
    el.wait_for_seconds(1)
    displayMessage(CROSS)
    lastmean = np.mean(VASes[-2:])
    laststd  = np.std(VASes[-2:])
    logger.writeLog(trial_number=trial_counter,
                    applied_temperature=temp_adjusted,
                    timing_starttrial=time()-time_beginning_of_experiment,
                    VAS_intensity=VAS,
                    VAS_unpleasanteness='NA')
    keepgoingcondition = len(VASes) < 2 or (lastmean < TARGET_VAS-10) or (lastmean > TARGET_VAS+10) or laststd > 8
    while keepgoingcondition:
        trial_counter += 1
        if lastmean < TARGET_VAS - 10:
            deltaT = min(0.02 * abs(VAS - TARGET_VAS), 0.5)
            temp_adjusted_new = temp_adjusted + deltaT
            print(f'last VAS < {TARGET_VAS - 10}, new adjusted temp: {temp_adjusted_new}')
        elif lastmean > TARGET_VAS + 10:
            deltaT = -min(0.02 * abs(VAS - TARGET_VAS), 0.5)
            temp_adjusted_new = temp_adjusted + deltaT
            print(f'last VAS > {TARGET_VAS + 10}, new adjusted temp: {temp_adjusted_new}')
        else:
            # if we're very close, we can still try to improve slightly ..maybe???
            deltaT = 0.01 * (TARGET_VAS - VAS) # will be max 0.01°C by the conditions above
            temp_adjusted_new = temp_adjusted
        print(f'deltaT: {deltaT}')
        # the vas search program cumulates the deltaTs
        time_of_plateau = time() - time_beginning_of_experiment
        makeTemperaturePlateau(deltaT, temp_adjusted_new, LENGTH, RATE, el)
        temp_adjusted = temp_adjusted_new
        VAS           = recordPain(screen, el, maxtime=3)
        VASes.append(VAS)
        # change the "-1" down there to average over more trials, eg -1 ---> -2 to average over the 2 last trials.
        lastmean = np.mean(VASes[-1:])
        laststd  = np.std(VASes[-1:])
        logger.writeLog(trial_number=trial_counter,
                        applied_temperature=temp_adjusted_new,
                        timing_starttrial=time_of_plateau,
                        VAS_intensity=VAS,
                        VAS_unpleasanteness='NA')
        keepgoingcondition = len(VASes) < 2 or (lastmean < TARGET_VAS-10) or (lastmean > TARGET_VAS+10) or laststd > 8
        if keepgoingcondition:
            displayMessage('Gleich startet der nächste Reiz')
            el.wait_for_seconds(1)
            displayMessage(CROSS)
        else:
            displayMessage('Danke')
            el.wait_for_seconds(1)
        
    saveThresholds(adjusted_temperature=temp_adjusted)
    return temp_adjusted 

    
    

    
    
    

if __name__ == "__main__":
    try:
        temp_adjusted = adjustement()
    except Exception as e:
        print(e)
        import traceback
        print(traceback.print_exc())
        pass
    finally:
        sendCommand('stop')
        pygame.quit()