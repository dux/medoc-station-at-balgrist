#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 13:44:46 2020

@author: Frédéric Dux
"""
from   time   import  time
# numerical python, to hande everything numbers related:
import numpy  as      np

# pygame, to display stuff:
import pygame
import pygame.font

from   pyparadigm.eventlistener       import  EventListener


# our very own ways of controlling the medoc station:
from   medoc_control                  import  launchMedocProgram, sendCommand, \
                                              holdMedocStation
# our parameters to the experiment:
from   config                         import  config
# our loggers:
from   loggers                        import  experimentLogger
# our other functions:
from   utilities                      import  DetectSpacebarPress, displayMessage,\
                                              displayError, readTrialsForAvoidance,\
                                              prepareDisplay, Pause, readIntensitiesForAvoidance,\
                                              saveSequenceOfRewards, loadSequenceOfReward,\
                                              makeFilenameForSequenceOfRewards
from   slider                         import  recordPain, recordDiscomfort

screen = prepareDisplay()
elcommand = EventListener()
cueToReactionTime = {
   'safe': 4.0,
   'easy': 0.5,
   'difficult': 0.5
}

cueToTranslation = {
   'safe': "sicher",
   'easy': "leicht",
   'difficult': "schwer"
}

# make an experiment logger:
logger = experimentLogger(extra_filename='avoidance')
# the participant id, session number are taken from config.txt. They can also be
# given as parameters: experimentLogger(participant_code='my code', session_number=42) ...

# filename where to save the sequence of successes:
pooldir  = "avoidance_reward_sequence_pool"
saved_sequence_of_reward_filename = makeFilenameForSequenceOfRewards(pooldir)

# factor out the launching of the program:
def launchForAvoidance(program_code, elcommand):
    resp = launchMedocProgram(program_code=program_code,
                              eventlistener=elcommand,
                              go_past_pretest=False)
    while resp != 0:
        displayError(elcommand)
        resp = launchMedocProgram(program_code=program_code,
                                      eventlistener=elcommand,
                                      go_past_pretest=False)
    holdMedocStation('IDLE', eventlistener=elcommand)
    # so apparently, no way of knowing whether the pretest is still
    # being ran or not. thus just wait for a while:
    elcommand.wait_for_seconds(2.5)



def avoidance(trials=readTrialsForAvoidance(), intensities=readIntensitiesForAvoidance(),\
              reinforcement_trial=-1,
              determined_reward_sequence="None", training=False, start_trial=0):
    """
       triallabels:   a list of labels corresponding to reaction times
                      above which the heat is triggered.

    """
    # by default, 'reward' is not set:
    reward = ''
    
    cumulative_reward_save = f"cumulative_reward_avoidance_{config.participant_code}.txt"
    try:
        cumulative_reward = float(np.loadtxt(cumulative_reward_save))
    except:
        cumulative_reward = 0.
    # make an event listener from pyParadigm, will be used to detect
    # all the inputs: (in particular the space bar presses)
    el = EventListener(permanent_handlers=(DetectSpacebarPress,))
    # another event listener that doesn't care whether we press the spacebar or not
    # counter for the trials:
    i = max(0, start_trial - 1)
    # a list to store the reaction times:
    reaction_times = []
    # keep track of whether the trial is repeated or not.
    repeatIteration = False
    # display some preliminary message:
    if training:
        message0  = """
Im Folgenden können Sie Schmerzreize vermeide, wenn Sie schnell genug reagieren.

Zu Beginn eines Durchgangs wir Ihnen auf dem Bildschirm angezeigt, wie schwer es sein
wird, die Schmerzreize zu vermeiden.
Es kann schwer, leicht oder sicher sein, wobei sicher bedeutet,
dass Sie ausreichend Zeit haben werden, zu reagieren. Sie sollen jedoch trotzdem
versuchen so schnell wie möglich zu reagieren.

Nach diesem Hinweise erscheint auf dem Bildschirm ein blauer Kreis.
Sobald Sie diesen Kreis sehen, müssen Sie so schnell wie möglich die Leertaste drücken.
Wenn Sie schnell genug sind und reagieren, solange der Kreis zu sehen ist,
vermeiden Sie den Schmerzreiz.
Waren Sie nicht schnell genug, erhalten Sie einen schmerzhaften Hitzereiz.

Zum Fortsetzen, bitte die Leertaste drücken.
"""
        message0prime = """
Nach einer Weile werden Sie in manchen Durchgängen Geldgewinne erhalten.
Sollte dies der Fall sein, werden Ihnen diese nach Ihrer Reaktion auf dem
Bildschirm angezeigt.

Hin und wieder werden Sie gebeten, einzuschätzen, wie intensitv und
wie angenehm oder unangenehm Sie die Hitzereize empfinden.
Dazu erhalten Sie den Hitzereiz einmal, ohne dass Sie auf den blauen Kreis reagieren
müssen, und Ihnen werden nacheinander die beiden Skalen angezeigt.
Danach geht die Aufgabe weiter.

Es starten nun zunächst ein paar Trainingsdurchgänge,
um Sie mit der Aufgabe vertraut zu machen.
 
Zum Starten des Trainings, bitte die Leertaste drücken.
"""
    else:
        message0 = """
Das Training ist nun beendet.
Drücken Sie die Leertaste, um die Aufgabe zu beginnen.
"""
        message0prime = ""
    displayMessage(message0, align='left')
    el.wait_for_keys(pygame.K_SPACE)
    if message0prime:
        displayMessage(message0prime)
        el.wait_for_keys(pygame.K_SPACE)
    displayMessage('')
    # this is the grand beginning:
    time_beginning_of_experiment = time()
    Ntrials = len(trials)

    # timer for breaks:
    is_it_time_for_break = time()
    # intensity:
    intensities = iter(intensities)
    # okay, gotta remove all the first elements from intensities:
    for _trial in range(i):
        if not trials[_trial] == 'vas':
            _ = next(intensities)
    # now we start with the right intensity, having removed the first N-1 trials. (N=start_trial)
    
    # redo as long as i < number of trials
    while i < Ntrials:
        did_not_react_in_time = False # reset this one, set to true lower if that's the case.
        reward = '' 
        # check if it is time for a break:
        if (time() - is_it_time_for_break) / 60 > 15 and (Ntrials-i)>10:
            Pause(elcommand, minseconds=30)
            is_it_time_for_break = time()
        # trial type:
        triallabel  = trials[i]
        if triallabel == 'vas':
            displayMessage('', background_color='lightbackground')
            order_intensities = ['hoch', 'niedrig']
            np.random.shuffle(order_intensities)
            for intensity in order_intensities:
                displayMessage('', background_color='lightbackground')
                # in case of vas trial, launch the strongest program:
                if intensity == 'hoch':
                    program_code = config.avoidance_strong
                else:
                    program_code = config.avoidance
                launchForAvoidance(program_code, elcommand)
                # and trigger the heat:
                sendCommand('trigger')
                # allow the trial to complete on the station:
                elcommand.wait_for_seconds(5)
                VAS_intensity = recordPain(screen, el, background_color='lightbackground')
                elcommand.wait_for_seconds(0.1)
                VAS_unpleasanteness = recordDiscomfort(screen, el, background_color='lightbackground')
                comment = f'vas only: intensity {intensity}'
                logger.writeLog(trial_number=i+1,
                    VAS_intensity=VAS_intensity,
                    VAS_unpleasanteness=VAS_unpleasanteness,
                    comments=comment)
            i += 1
            continue # skip the rest of the trial.
        if not repeatIteration:
            intensity   = next(intensities)
        else: # if repeatIteration, well skip the next(intensities) and set it back to False.
            repeatIteration = False
        if intensity == 'hoch':
            program_code = config.avoidance_strong
        else:
            program_code = config.avoidance
        limit       = cueToReactionTime[triallabel]
        # now we start the trial on the medoc station, so that it's ready to
        # go when needed:
        timing_starttrial = time() - time_beginning_of_experiment
        
        # launching the program:
        launchForAvoidance(program_code, elcommand)

        if i == 0:
            # if first time, wait a longer time such that the participant can read calmly:
            wait_time = np.random.randint(3000, 4000)
        else:
            # lasts randomly between 1 and 4 seconds before displaying the disk:
            wait_time = np.random.randint(1000, 4000)
        # the event listener listens for all inputs:
        cue_duration = np.random.randint(999, wait_time)
        A = el.listen_until_return(timeout=wait_time/1000. - cue_duration/1000.)
        if not A == "pressed":
            displayMessage(f"{cueToTranslation[triallabel]} \n\n\n ({intensity})")
            timing_cue = time() - time_beginning_of_experiment
            B = el.listen_until_return(timeout=cue_duration/1000)
        # if it detected a space bar press:
        if A == "pressed" or B == "pressed":
            # not good, the trial is not valid. Shows this on the screen:
            displayMessage("zu früh, Durchgang wird wiederholt")
            repeatIteration = True
            sendCommand('stop')
            logger.writeLog(trial_number=i+1,
                            timing_starttrial=timing_starttrial,
                            comments="space bar pressed too fast.",
                            VAS_intensity='NA',
                            VAS_unpleasanteness='NA',
                            condition=triallabel)
            elcommand.wait_for_seconds(1)
            resp = holdMedocStation('TEST IN PROGRESS', 'RUNNING')
            if resp != 0:
                displayError(elcommand)
            elcommand.wait_for_seconds(1)
            # restart at the beginning of the "while" loop with the "continue" keyword:
            continue
        # if no buttons were pressed, we display the disk:
        displayMessage("", 'blue')
        timing_target = time() - time_beginning_of_experiment
        # and mark the beginning of the chronometer:
        t0 = time()
        # we listen for the input of the participant: (after 5 seconds we stop,
        # no point getting stuck waiting)
        A = el.listen_until_return(timeout=limit)
        displayMessage("")
        if not A == 'pressed':
            A = el.listen_until_return(timeout=3)
        # t is the time in seconds that was used by the participant to press space:
        reaction_time = time() - t0
        # we also record the absolute timing since the grand beginning:
        timing_response = time() - time_beginning_of_experiment
        if A == 'pressed':
            i += 1
            trialnumber = i
            # store the reaction time in our list:
            reaction_times.append(reaction_time)
            success = reaction_time <= limit
            
            # store the success for use in another experiment:
            if i <= reinforcement_trial:
                saveSequenceOfRewards([success], pooldir, filename=saved_sequence_of_reward_filename)
            
            if reinforcement_trial >= 0 and i > reinforcement_trial:
                # if we do reinforcement, we may or not receive a reward
                # depending on the phase of the trial.
                # Either the rewards are given based on a sequence:
                if determined_reward_sequence != "None":
                    reward = determined_reward_sequence[i-reinforcement_trial-1]
                else:
                    # or the reward is simply based on the success:
                    # 'true' if the reaction time is below the limit.
                    reward = int(success)
                 
            else: # if we don't do reinforcement, we never hand out rewards
                reward = 0
            if not success:
                # then we use the medoc to increase the temperature
                sendCommand('trigger')
                if reward:
                    displayMessage("Gewonnen! \n 0.20CHF")
                    el.wait_for_seconds(4)
                    displayMessage('')
            else:
                sendCommand('stop')
                if reward:
                    displayMessage("Gewonnen! \n 0.20CHF")
                    cumulative_reward += 0.2
                    np.savetxt(cumulative_reward_save, [cumulative_reward])
                    el.wait_for_seconds(4)
                    displayMessage('')
                
            resp = holdMedocStation('TEST IN PROGRESS', 'RUNNING')
            if resp != 0:
                displayError(elcommand)
        else:
            # now if the spacebar was not pressed:
            repeatIteration = True
            did_not_react_in_time = True
            displayMessage(f"zu langsam, Durchgang wird wiederholt")
            sendCommand('trigger')
            resp = holdMedocStation('TEST IN PROGRESS', 'RUNNING')
            if resp != 0:
                displayError(elcommand)
            trialnumber = i+1


        VAS_intensity, VAS_unpleasanteness = 'NA', 'NA'
        # if we reached this point in the while loop, everything went well:
        # we can log everything.
        comment = []
        if training:
            comment.append("training")
        if did_not_react_in_time:
            comment.append("did_not_react_in_time")
        comment = ','.join(comment)
        logger.writeLog(trial_number=trialnumber,
                        reaction_time=reaction_time,
                        limit_time=limit,
                        timing_starttrial=timing_starttrial,
                        timing_cue=timing_cue,
                        timing_target=timing_target,
                        timing_response=timing_response,
                        condition=triallabel,
                        VAS_intensity='NA',
                        VAS_unpleasanteness='NA',
                        reward=reward,
                        cumulative_reward=cumulative_reward,
                        comments=comment)
        # as usual, wait as to not overwhelm the station
        elcommand.wait_for_seconds(1.5)
    el.wait_for_seconds(1)
    return np.array(reaction_times)





###################### RUNNING THE ROUTINE WE JUST MADE #######################

# again, just a statement saying that we execute the above only if
# we run things from this file. ('if name of file is equal to main file')
if __name__ == "__main__":

    # try, and if there is an error, no problem, just do what is after
    # "except" below. (this try, except, finally thing is just a precaution
    # and there is nothing special to understand here.)
    try:
        if config.avoidance_do_training.lower() == 'yes' and config.avoidance_start_trial <= 1:
            # first the training
            trials = ['easy', 'safe', 'safe', 'difficult', 'difficult', 'easy', \
                      'easy', 'difficult', 'easy', 'safe', 'difficult', 'safe']
            # random order:
            # np.random.shuffle(trials) # commented out, needs the same for all participants.
            reactiontimes = avoidance(trials=trials, intensities=len(trials)*['hoch'], training=True)
            # (the intensities provided to the training is a decoy, won't be used in training mode)

            # now adapt the reaction times:
            # (watch out, this method assumes the reaction time are normally distributed.
            # It is not the case as in they are in fact exponential-gaussian distributed.)
            reactiontimes = np.array(reactiontimes)
            # filter the really slow responses:
            filtered = reactiontimes[reactiontimes<=0.5]
            # filter the obvious outliers:
            def removeOutliers(reactiontimes, N=2):
                return reactiontimes[abs(reactiontimes - np.mean(reactiontimes)) < N * np.std(reactiontimes)]
            filtered = removeOutliers(filtered)
            if len(filtered) < 4:
                # less aggressive filtering then
                filtered = removeOutliers(filtered, N=1)
            # read off the limits:
            t_easy = np.percentile(filtered, 66.6)
            t_difficult = np.percentile(filtered, 33.3)
            print(f"The personalized reaction time are: {t_easy:.02f} and {t_difficult:.02f} seconds.")
            cueToReactionTime['easy'] = t_easy
            cueToReactionTime['difficult'] = t_difficult
            np.savetxt(f"{config.participant_code}_reaction_times.txt", [t_easy, t_difficult])
        
        # if we restart, we must read the reaction times from the training 
        if config.avoidance_start_trial > 0:
            t_easy, t_difficult = np.loadtxt(f"{config.participant_code}_reaction_times.txt")
            cueToReactionTime['easy'] = t_easy
            cueToReactionTime['difficult'] = t_difficult
        # then the main part, reading the trials off the file.
        number_of_trials_before_reinforcement = config.avoidance_number_of_trials_before_reinforcement
        # do we use a predetermined sequence?
        usepredeterminedsequence = config.avoidance_use_predetermined_sequence
        if usepredeterminedsequence.lower() == 'yes':
            predeterminedsequence = loadSequenceOfReward(pooldir, config.avoidance_file_with_predetermined_sequence)
        else:
            predeterminedsequence = "None"
        reactiontimes = avoidance(trials=readTrialsForAvoidance(),\
                                  intensities=readIntensitiesForAvoidance(),\
                                  reinforcement_trial=number_of_trials_before_reinforcement,\
                                  determined_reward_sequence=predeterminedsequence,\
                                  start_trial=config.avoidance_start_trial)
        displayMessage('Danke')
        elcommand.wait_for_seconds(4)

    except Exception as e:
        # if there is an error, display it
        print(e)
        import traceback
        traceback.print_exc()
    finally:
        # finally, even if the worst happens, still stop pygame so that we
        # don't get stuck with a frozen screen:
        sendCommand('stop')
        pygame.quit()
