#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 12:27:40 2020

@author: frederic
"""
def readconfig(wantedkey):
    try:
        with open('config.txt', 'r') as f:
            for line in f.readlines():
                line = line.strip()
                if not line or line.startswith('#'):
                    continue
                key, val = line.split(':')
                key, val = key.strip(), val.strip()
                if key == wantedkey:
                    return val
    except Exception as e:
        print("error:", e, ", with keyword", wantedkey)
        return input(f"couldn't read the parameter {wantedkey}, enter it please: ")

class Config:
    address = readconfig('address') 
    port    = int(readconfig('port'))
    temperatureRate = float(readconfig('temperatureRate'))
    temperatureRateAdjustment = float(readconfig('temperatureRateAdjustment'))
    
    avoidance      = readconfig('avoidance_code')
    avoidance_strong = readconfig('avoidance_strong_code')
    discrimination = readconfig('discrimination_code')
    adjustment     = readconfig('adjustment_code')
    adjustment_plateau_length = float(readconfig('adjustment_plateau_length'))
    adjustment_target_vas = float(readconfig('adjustment_target_vas'))
    adjustement_baseline_temperature = float(readconfig('adjustement_baseline_temperature'))
    thresholds     = readconfig('thresholds_code')
    adjustment     = readconfig('adjustment_code')
    fixedstimuli   = readconfig('fixedstimuli_code')
    participant_code = readconfig('participant_code')
    session_number = readconfig('session_number')
    timedelayformedoc = 0.1
    debug = 0
    file_with_avoidance_trials = readconfig('file_with_avoidance_trials')
    file_with_discrimination_trials = readconfig('file_with_discrimination_trials')
    record_of_thresholds = readconfig('record_of_thresholds')
    
    
    discrimination_use_predetermined_sequence = readconfig('discrimination_use_predetermined_sequence')
    discrimination_file_with_predetermined_sequence = readconfig('discrimination_file_with_predetermined_sequence')
    discrimination_number_of_trials_before_reinforcement = int(readconfig('discrimination_number_of_trials_before_reinforcement'))
    discrimination_start_trial = int(readconfig('discrimination_start_trial'))
    
    avoidance_use_predetermined_sequence = readconfig('avoidance_use_predetermined_sequence')
    avoidance_file_with_predetermined_sequence = readconfig('avoidance_file_with_predetermined_sequence')
    avoidance_number_of_trials_before_reinforcement = int(readconfig('avoidance_number_of_trials_before_reinforcement'))
    avoidance_do_training = readconfig('avoidance_do_training')
    avoidance_start_trial = int(readconfig('avoidance_start_trial'))
    file_with_avoidance_intensities = readconfig('file_with_avoidance_intensities')
    
    

config = Config()
