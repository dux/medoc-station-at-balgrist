#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  29 19:55:41 2020

@author: fred
"""
import  pygame
import  numpy  as      np
from    time   import  time


from utilities import titlefont, littlefont, mediumfont, littleboldfont, colors



class SliderVAS():
    def __init__(self, question, text1, text2, text3, valstart, valmin, valmax, screen):
        self.val  = valstart
        self.maxi = valmax      
        self.mini = valmin
        self.X, self.Y = screen.get_size()
        self.xpos = self.X//6
        self.ypos = self.Y//3
        self.width= int(self.X*2/3)
        self.screen = screen
        
        self.cursor_speed = 0
        self.accumulation = 0
        self.time = 0
        self.question = question
        self.text1 = text1
        self.text2 = text2
        self.text3 = text3
        
        # dynamic graphics - button surface #
        self.sliderbutton = pygame.surface.Surface((5, 80))
        self.sliderbutton.fill(colors['orange'])
        self.draw()

    def draw(self):
        """ Combination of static and dynamic graphics in a copy of
            the basic slide surface
        """
        # big text:
        title = titlefont.render(self.question, True, colors['white'])
        titlewidth = title.get_rect().width 
        self.screen.blit(title, [self.X//2 - titlewidth//2, 60])
        # text and bars
        yoffset = 50
        xoffset = -15
        text = littlefont.render(self.text1, True, colors['white'])
        text = pygame.transform.rotate(text, -60)
        self.screen.blit(text, [self.xpos+xoffset, self.ypos+yoffset])
        text = littleboldfont.render(self.text2, True, colors['white'])
        text = pygame.transform.rotate(text, -60)
        self.screen.blit(text, [self.xpos+self.width//2+xoffset, self.ypos+yoffset])
        text = littlefont.render(self.text3, True, colors['white'])
        text = pygame.transform.rotate(text, -60)
        self.screen.blit(text, [self.xpos+self.width+xoffset, self.ypos+yoffset])
        bar  = pygame.rect.Rect(pygame.rect.Rect(self.xpos, self.ypos+3, self.width, 6))
        pygame.draw.rect(self.screen, colors['white'], bar)
        bar1 = pygame.rect.Rect(pygame.rect.Rect(self.xpos-2, self.ypos-3, 5, 20))
        pygame.draw.rect(self.screen, colors['white'], bar1)
        bar2 = pygame.rect.Rect(pygame.rect.Rect(self.xpos+self.width//2-2, self.ypos-11, 5, 36))
        pygame.draw.rect(self.screen, colors['white'], bar2)
        bar3 = pygame.rect.Rect(pygame.rect.Rect(self.xpos+self.width-2, self.ypos-3, 5, 20))
        pygame.draw.rect(self.screen, colors['white'], bar3)
        # values:
        xoffset2 = -47
        yoffset2 = -50
        intermediate = (self.maxi + self.mini) // 2
        v1 = littlefont.render(f"{self.mini:>6.0f}", True, colors['white'])
        self.screen.blit(v1, [self.xpos+xoffset2, self.ypos+yoffset2])
        v2 = littlefont.render(f"{intermediate:>6.0f}", True, colors['white'])
        self.screen.blit(v2, [self.xpos+self.width//2+xoffset2, self.ypos+yoffset2])
        v3 = littlefont.render(f"{self.maxi:>6.0f}", True, colors['white'])
        self.screen.blit(v3, [self.xpos++self.width+xoffset2, self.ypos+yoffset2])
        # the cursor now:
        pos = (self.xpos + int((self.val-self.mini)/(self.maxi-self.mini)*self.width), self.ypos+5)
        self.button_rect = self.sliderbutton.get_rect(center=pos)
        self.screen.blit(self.sliderbutton, self.button_rect)

        # arrows at the bottom:  (removed)
        # arrows = mediumfont.render('←   use the arrows on the keyboard   → ',True, colors['white'])
        # arrowidth  = arrows.get_rect().width
        # arrowheight = arrows.get_rect().height
        # self.screen.blit(arrows, [self.xpos+self.width//2 - arrowidth//2, self.Y-2*arrowheight])

    def move(self):
        acc_trehshold = 10
        if self.time == 0:
            self.time = time()
            dt = 0.05
        else:
            dt = time() - self.time
            self.time = time()
        keys = pygame.key.get_pressed()
        self.val += 4 * dt * self.cursor_speed
        if keys[pygame.K_LEFT] and keys[pygame.K_RIGHT]:
            self.cursor_speed = 0
            self.accumulation = 0
        elif keys[pygame.K_LEFT]:
            if self.cursor_speed > 0:
                self.cursor_speed = 0
                self.accumulation = 0
            if self.accumulation > acc_trehshold:
                self.cursor_speed -= 200 * dt
            else:
                self.cursor_speed -= 50 * dt
                self.accumulation += 1
        elif keys[pygame.K_RIGHT]:
            if self.cursor_speed < 0:
                self.cursor_speed = 0
                self.accumulation = 0
            if self.accumulation > acc_trehshold:
                self.cursor_speed += 200 * dt
            else:
                self.cursor_speed += 50 * dt
                self.accumulation += 1
        else:
            self.cursor_speed = 0
            self.accumulation = 0
        
        if np.abs(self.cursor_speed) > 90:
            self.cursor_speed = np.sign(self.cursor_speed)*90
        if self.val < self.mini:
            self.val = self.mini
        if self.val > self.maxi:
            self.val = self.maxi
           



def recordVASWithSlider(question, text1, text2, text3, 
                        startvalue, valmin, valmax, 
                        maxtime, eventlistener,
                        screen, extra_key_stop=None, background_color="background"):
    slider = SliderVAS(question, text1, text2, text3, startvalue, valmin, valmax, screen)
    t0 = time()
    keepgoing = True
    while time()-t0 <= maxtime and keepgoing:
        slider.move()
        # Update screen
        screen.fill(colors[background_color])
        slider.draw()
        pygame.display.flip()
        event = eventlistener.wait_for_seconds(1./80.)
        if extra_key_stop:
            keys = pygame.key.get_pressed()
            if time() - t0 > 0.5:
                if event or keys[extra_key_stop]:
                    keepgoing = False
    screen.fill(colors['background'])
    pygame.display.flip()
    return slider.val

def recordPain(screen, eventlistener, maxtime=4, question='', **kwargs):
    if not question:
        question = "Intensität"
    text1    = 'keine Empfindung'
    text2    = 'Schmerzschwelle'
    text3    = 'stärkster tolerierbarer Schmerz'
    startvalue = 100
    valmin   = 0
    valmax   = 200 
    score    = recordVASWithSlider(question, text1, text2, text3, \
                                   startvalue, valmin, valmax, maxtime, \
                                   eventlistener, screen, **kwargs)
    return score

def recordDiscomfort(screen, eventlistener, maxtime=4, question='', **kwargs):
    if not question:
        question   = "Un-/Angenehmheit"
    text1      = 'extrem unangenehm'
    text2      = 'neutral'
    text3      = 'extrem angenehm'
    startvalue = 0
    valmin     = -100
    valmax     = 100
    score      = recordVASWithSlider(question, text1, text2, text3, \
                                   startvalue, valmin, valmax, maxtime, \
                                   eventlistener, screen, **kwargs)
    return score

if __name__ == "__main__":
    from   pyparadigm.eventlistener  import  EventListener
    from   utilities import prepareDisplay
    screen = prepareDisplay()
    el = EventListener()
    recordPain(screen, el, maxtime=1000, extra_key_stop=pygame.K_SPACE)
    recordDiscomfort(screen, el, maxtime=1000, extra_key_stop=pygame.K_SPACE)
    pygame.quit()
    
""" GRAVEYARD
    from pyparadigm.misc import init
    from pyparadigm.eventlistener import  EventListener
    screen = init((1280, 720), pygame.HWSURFACE | pygame.DOUBLEBUF)
    answer = recordVASWithSlider("How painful was it?", 
                                'felt nothing', 'pain threshold', 'highest pain bearable', 
                                30, 0, 100, 120, EventListener(), screen)
    print(answer)
    pygame.quit()
    import sys
    sys.exit()
    



        # elif event.type == pygame.MOUSEBUTTONDOWN:
            # pos = pygame.mouse.get_pos()
                # if s.button_rect.collidepoint(pos):
                    # s.hit = True
        # elif event.type == pygame.MOUSEBUTTONUP:
            # for s in slides:
                # s.hit = False
                
"""
