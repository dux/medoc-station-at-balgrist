#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 15:04:05 2020

@author: frederic
"""

from   time   import  time
# numerical python, to hande everything numbers related:
import numpy  as      np

# pygame, to display stuff:
import pygame
import pygame.font

from   pyparadigm.eventlistener       import  EventListener


from    utilities       import      displayMessage, displayError, prepareDisplay,\
                                    DetectSpacebarPress, saveThresholds,\
                                    loadRecordOfThresholds
from    medoc_control   import      launchMedocProgram, sendCommand
from    config          import      config
from    slider          import      recordPain, recordDiscomfort
from    loggers         import      experimentLogger

logger = experimentLogger(extra_filename='fixed_stimuli')

screen = prepareDisplay()
CROSS = "+"

def fixedStimuli():
    displayMessage('(loading)')
    launchMedocProgram(config.fixedstimuli)
    el = EventListener()
    # give the station time to initialize:
    el.wait_for_seconds(3)
    pains = []
    discomforts = []
    message  = "Sie werden nun mehrer kurze Hitzereize erhalten. Sie dabei für jeden dieser Reize\n"
    message += "einschätzen, wie intensiv und wie unangenehm oder angenehm Sie diese empfunden haben.\n"
    message += "Dazu werden Ihnen nach dem Reiz beide Skalen nacheinander auf dem Bildschirm angezeigt.\n\n"
    message += "Drücken Sie die Leertaste, um zu beginnen."
    displayMessage(message)
    el.wait_for_keys(pygame.K_SPACE)
    displayMessage('')
    time_beginning_of_experiment = time()
    # displayMessage('In this experiment, you will be \n subjected to various temperatures.')
    # el.wait_for_keys(pygame.K_SPACE)
    # displayMessage('For each of them, you will be asked to rate \n\n \t i) how painfull \n \t ii) how uncomfortable \n\n it felt.')
    # el.wait_for_keys(pygame.K_SPACE)
    # displayMessage('Press the space bar to start.')
    # el.wait_for_keys(pygame.K_SPACE)
    # displayMessage('')
    for i in range(7):
        displayMessage(CROSS)
        el.wait_for_seconds(np.random.uniform(1, 3))
        sendCommand('trigger')
        timing_starttrial = time()-time_beginning_of_experiment
        el.wait_for_seconds(4.5)
        pain = recordPain(screen, el)
        displayMessage('')
        el.wait_for_seconds(0.05)
        pains.append(pain)
        discomfort = recordDiscomfort(screen, el)
        discomforts.append(discomfort)
        logger.writeLog(
                    trial_number=i+1,
                    timing_starttrial=timing_starttrial,
                    VAS_intensity=pain,
                    VAS_unpleasanteness=discomfort)
        if i < 6:
            displayMessage('Gleich startet der nächste Reiz.')
            
        else:
            displayMessage('Danke')
        el.wait_for_seconds(1)
        displayMessage('')
    return pains, discomforts
        
    
    
    
    

if __name__ == "__main__":
    try:
        fixedStimuli()
    except Exception as e:
        print(e)
    finally:
        sendCommand('stop')
        pygame.quit()